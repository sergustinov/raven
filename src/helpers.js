export default {
    resolveSvgPath(path) {
        let icons = require.context("@/SVG/", false, /\.svg$|\.jpg$|\.png$/);
        return icons("./" + path);
    }
}
