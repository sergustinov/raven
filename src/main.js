import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueMuuri from 'vue-muuri';
import 'vue-muuri/dist/vue-muuri.css';
import helpers from './helpers.js'
import vSelect from 'vue-select'

const plugin = {
	install () {
		Vue.helpers = helpers
		Vue.prototype.$helpers = helpers
	}
}

Vue.use(plugin)

const arrowDownPath = helpers.resolveSvgPath('arrow-down.svg')

vSelect.props.components.default = () => ({
	OpenIndicator: {
		render: createElement => createElement('img', {
			attrs: {
				src: arrowDownPath
			}
		}),
	},
});

Vue.component('v-select', vSelect)



Vue.use(VueMuuri);

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount('#app');
